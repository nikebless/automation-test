const webpack = require('webpack');
const path = require('path');

// Don't open the browser during development
process.env.BROWSER = 'none';

module.exports = {
  webpack: {
    alias: { '@ant-design/icons/lib/dist$': path.resolve(__dirname, './src/icons.js') },
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      })
    ],
    configure: {
      optimization: {
        splitChunks: {
          cacheGroups: {
            default: false,
          },
        },
        runtimeChunk: false
      }
    }
  },
};
