const axios = require('axios')
const glob = require("glob")
const fs = require('fs')

const INSTANCE_URL = process.env.REACT_APP_INSTANCE_URL || 'https://dev49499.service-now.com'

const USERNAME = process.env.REACT_APP_USER || 'admin'
const PASSWORD = process.env.REACT_APP_PASSWORD || 'admin'

const JS_FILE_ID = '425ac1944f703300259989dba310c7a4'
const CSS_FILE_ID = 'c3aa45944f703300259989dba310c7aa'

axios.defaults.headers.put["Content-Type"] = "application/json";
axios.defaults.auth = {
    username: USERNAME,
    password: PASSWORD,
}
axios.defaults.baseURL = INSTANCE_URL

// getting JS contents
glob('build/static/js/main.*.js', (err, files) => {
    let contents = fs.readFileSync(files[0]).toString()
    pushCodeResource(JS_FILE_ID, contents)
})

// getting CSS contents
glob('build/static/css/main.*.css', (err, files) => {
    let contents = fs.readFileSync(files[0]).toString()
    pushCodeResource(CSS_FILE_ID, contents)
})

function pushCodeResource(id, contents) {
    // contents = encodeURIComponent(contents)
    const data = { "style": contents }
    axios
        .put('/api/now/table/content_css/' + id, data)
        .catch(err => {
            console.log(err.response.data.error)
        })
}




