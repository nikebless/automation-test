import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <p>
            Hello world!
          </p>
          <p>
            This is the first change with continous deployment.
          </p>
        </header>
      </div>
    );
  }
}

export default App;
